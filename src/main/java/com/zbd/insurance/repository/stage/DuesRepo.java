package com.zbd.insurance.repository.stage;

import com.zbd.insurance.entity.stage.Dues;
import com.zbd.insurance.entity.stage.Travel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DuesRepo extends JpaRepository<Dues, Long> {
}
