package com.zbd.insurance.repository.stage;

import com.zbd.insurance.entity.stage.IssueStage;
import com.zbd.insurance.entity.stage.Travel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IssueStageRepo extends JpaRepository<IssueStage, Long> {
}
