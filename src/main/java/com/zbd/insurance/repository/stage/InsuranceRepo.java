package com.zbd.insurance.repository.stage;

import com.zbd.insurance.entity.stage.Insurance;
import com.zbd.insurance.entity.stage.Travel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceRepo extends JpaRepository<Insurance, Long> {
}
