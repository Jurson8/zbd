package com.zbd.insurance.repository.stage;

import com.zbd.insurance.entity.stage.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepo extends JpaRepository<Client, Long> {
}
