package com.zbd.insurance.repository.stage;

import com.zbd.insurance.entity.stage.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleRepo extends JpaRepository<Vehicle, Long> {
}
