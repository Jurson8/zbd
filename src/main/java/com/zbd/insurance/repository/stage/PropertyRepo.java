package com.zbd.insurance.repository.stage;

import com.zbd.insurance.entity.stage.Property;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyRepo extends JpaRepository<Property, Long> {
}
