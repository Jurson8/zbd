package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;


//@Entity
@Data
@Table(name = "car")
@Builder
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_sequence")
    @SequenceGenerator(name = "car_sequence", allocationSize = 1)
    private Long id;

    private String brand;

    private String model;

    private Integer hp;

    private Integer doors;

    private Integer people;

    private Integer weight;

    private Integer mileage;

    private LocalDate prodDate;

    private LocalDate firstRegistration;

    private String vin;

    private String registrationNumber;
}
