package com.zbd.insurance.entity.stage;

import lombok.Builder;
import lombok.Data;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "dues")
@Builder
public class Dues {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dues_sequence")
    @SequenceGenerator(name = "dues_sequence", allocationSize = 1)
    private Long id;

    private BigDecimal amount;

    @ManyToOne
    private Insurance insurance;

}
