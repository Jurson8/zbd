package com.zbd.insurance.entity.stage;

import com.zbd.insurance.entity.Address;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "property")
@Builder
public class Property {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "property_sequence")
    @SequenceGenerator(name = "property_sequence", allocationSize = 1)
    private Long id;

    @Embedded
    private Address address;

    private Integer area;

    @ManyToOne
    private Insurance insurance;

}
