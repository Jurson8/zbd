package com.zbd.insurance.entity.stage;


import com.zbd.insurance.entity.Issue;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "insurance")
@Builder
public class Insurance {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "insurance_sequence")
    @SequenceGenerator(name = "insurance_sequence", allocationSize = 1)
    private Long id;

    private String insuranceSubject;

    private Integer insuranceValue;

    @OneToMany(mappedBy = "insurance", cascade = CascadeType.ALL)
    private List<IssueStage> issues;

    @OneToMany(mappedBy = "insurance", cascade = CascadeType.ALL)
    private List<Dues> dues;

    @OneToMany(mappedBy = "insurance", cascade = CascadeType.ALL)
    private List<Travel> travels;

    @OneToMany(mappedBy = "insurance", cascade = CascadeType.ALL)
    private List<Property> properties;

    @OneToMany(mappedBy = "insurance", cascade = CascadeType.ALL)
    private List<Vehicle> vehicles;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Client> clients;

    public void addIssue(IssueStage issueStage) {
        this.issues = this.issues == null? new ArrayList<>() : this.issues;
        issueStage.setInsurance(this);
        this.issues.add(issueStage);
    }

    public void addDues(Dues dues) {
        this.dues = this.dues == null? new ArrayList<>() : this.dues;
        dues.setInsurance(this);
        this.dues.add(dues);
    }

    public void addClient(Client client) {
        this.clients = this.clients == null? new ArrayList<>() : this.clients;
        client.addInsurance(this);
        this.clients.add(client);
    }

    public void addVehicle(Vehicle vehicle) {
        this.vehicles = this.vehicles == null? new ArrayList<>() : this.vehicles;
        vehicle.setInsurance(this);
        this.vehicles.add(vehicle);
    }

    public void addTravel(Travel travel) {
        this.travels = this.travels == null? new ArrayList<>() : this.travels;
        travel.setInsurance(this);
        this.travels.add(travel);
    }

    public void addProperty(Property property) {
        this.properties = this.properties == null? new ArrayList<>() : this.properties;
        property.setInsurance(this);
        this.properties.add(property);
    }


}
