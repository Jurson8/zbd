package com.zbd.insurance.entity.stage;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "compensation")
@Builder
public class Compensation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "compensation_sequence")
    @SequenceGenerator(name = "compensation_sequence", allocationSize = 1)
    private Long id;

    private BigDecimal amount;

    @OneToOne(cascade = CascadeType.ALL)
    private IssueStage issue;
}
