package com.zbd.insurance.entity.stage;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Data
@Table(name = "vehicle")
@Builder
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vehicle_sequence")
    @SequenceGenerator(name = "vehicle_sequence", allocationSize = 1)
    private Long id;

    private String brand;

    private Integer power;

    private Integer doors;

    private Integer people;

    private Integer weight;

    private Integer mileage;

    private LocalDate prodDate;

    private String vin;

    private String registrationNumber;

    // typ nadwozia
    @Column(name = "type1")
    private String type;

    @ManyToOne
    private Insurance insurance;
}
