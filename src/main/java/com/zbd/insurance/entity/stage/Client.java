package com.zbd.insurance.entity.stage;

import com.zbd.insurance.entity.Address;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "client")
@Builder
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_sequence")
    @SequenceGenerator(name = "client_sequence", allocationSize = 1)
    private Long id;

    private Address address;

    @Column(name = "name1")
    private String name;

    private String surname;

    private LocalDate birthDate;

    private Boolean sex;

    @ManyToMany(mappedBy = "clients", cascade = CascadeType.ALL)
    private List<Insurance> insurance;

    public void addInsurance(Insurance insurance) {
        this.insurance = this.insurance == null ? new ArrayList<>() : this.insurance;
        this.insurance.add(insurance);
    }


}
