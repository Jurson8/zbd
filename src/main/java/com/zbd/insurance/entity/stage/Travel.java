package com.zbd.insurance.entity.stage;


import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "travel")
@Builder
public class Travel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "travel_sequence")
    @SequenceGenerator(name = "travel_sequence", allocationSize = 1)
    private Long id;

    private Integer duration;

    private String country;

    @ManyToOne
    private Insurance insurance;
}
