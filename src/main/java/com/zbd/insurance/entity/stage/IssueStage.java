package com.zbd.insurance.entity.stage;


import com.zbd.insurance.entity.Address;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "issue_stage")
@Builder
public class IssueStage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issue_stage_sequence")
    @SequenceGenerator(name = "issue_stage_sequence", allocationSize = 1)
    private Long id;

    @Embedded
    private Address address;

    @Column(name = "date1")
    private LocalDate date;

    @Column(name = "type1")
    private String type;

    @OneToOne(cascade = CascadeType.ALL)
    private Compensation compensation;

    @ManyToOne
    private Insurance insurance;
}
