package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

//@Entity
@Data
@Table(name = "issue")
@Builder
public class Issue {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "issue_sequence")
    @SequenceGenerator(name = "issue_sequence", allocationSize = 1)
    private Long id;

    private String issueType;

    private String issueSubject;

    private BigDecimal issueCost;

    private LocalDate issueDate;

    @ManyToOne
    private CarInsurance carInsurance;

    @ManyToOne
    private HouseInsurance houseInsurance;

    @ManyToOne
    private HealthInsurance healthInsurance;

    @Embedded
    private Address issuePlace;

    private String sufferedPolicyNumber;
}
