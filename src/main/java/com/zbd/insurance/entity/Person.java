package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

//@Entity
@Data
@Table(name = "person")
@Builder
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "person_sequence")
    @SequenceGenerator(name = "person_sequence", allocationSize = 1)
    private Long id;

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

    // false - man, true - woman
    private Integer sex;

    private String telephone;

    private String personType;

    private Address address;

    private String email;

    @OneToMany(mappedBy = "owner")
    private List<CarInsurance> carInsurance;
}
