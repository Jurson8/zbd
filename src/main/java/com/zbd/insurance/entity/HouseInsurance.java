package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

//@Entity
@Data
@Table(name = "house_insurance")
@Builder
public class HouseInsurance {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "house_insurance_sequence")
    @SequenceGenerator(name = "house_insurance_sequence", allocationSize = 1)
    private Long id;

    private String insuranceType;

    private BigDecimal cost;

    private String packet;

    private BigDecimal guarantee;

    private LocalDate startDate;

    private LocalDate endDate;

    @ManyToOne
    private Person owner;

    @OneToOne
    private House house;

    @OneToMany(mappedBy = "houseInsurance", cascade = CascadeType.ALL)
    private List<Issue> issues;
}
