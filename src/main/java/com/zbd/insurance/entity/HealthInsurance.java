package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

//@Entity
@Data
@Table(name = "health_insurance")
@Builder
public class HealthInsurance {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "health_insurance_sequence")
    @SequenceGenerator(name = "health_insurance_sequence", allocationSize = 1)
    private Long id;

    private String insuranceType;

    private BigDecimal cost;

    private String packet;

    private BigDecimal guarantee;

    private LocalDate startDate;

    private LocalDate endDate;

    @ManyToOne
    private Person owner;

    @OneToMany(mappedBy = "healthInsurance", cascade = CascadeType.ALL)
    private List<PersonHealth> personHealth;

    @OneToMany(mappedBy = "healthInsurance")
    private List<Issue> issues;
}
