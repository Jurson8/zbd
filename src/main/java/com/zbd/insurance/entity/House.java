package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

//@Entity
@Data
@Table(name = "house")
@Builder
public class House {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "house_sequence")
    @SequenceGenerator(name = "house_sequence", allocationSize = 1)
    private Long id;

    private Integer area;

    private BigDecimal assessment;

    private LocalDate dateOfAssessment;

    @Embedded
    private Address address;

}
