package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

//@Entity
@Data
@Table(name = "car_insurance")
@Builder
public class CarInsurance {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_insurance_sequence")
    @SequenceGenerator(name = "car_insurance_sequence", allocationSize = 1)
    private Long id;

    private String insuranceType;

    private BigDecimal cost;

    private String packet;

    private BigDecimal guarantee;

    private LocalDate startDate;

    private LocalDate endDate;

    @ManyToOne
    private Person owner;

    @OneToOne
    private Car car;

    @OneToMany(mappedBy = "carInsurance")
    private List<Issue> issues;
}
