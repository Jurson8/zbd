package com.zbd.insurance.entity;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class Address {

    private String city;

    private String postalCode;

    private String street;

}