package com.zbd.insurance.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

//@Entity
@Data
@Builder
@Table(name = "person_health")
public class PersonHealth {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "health_person_sequence")
    @SequenceGenerator(name = "health_person_sequence", allocationSize = 1)
    private Long id;

    private String diseasePast;

    private String diseaseCurrent;

    private LocalDate lastTest;

    @ManyToOne
    private HealthInsurance healthInsurance;
}
