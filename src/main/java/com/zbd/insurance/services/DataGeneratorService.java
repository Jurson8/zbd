package com.zbd.insurance.services;

import com.zbd.insurance.dataset.DataSet;
import com.zbd.insurance.entity.*;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.zbd.insurance.dataset.DataSet.getRandomCarModels;

@Service
public class DataGeneratorService {

    private DataFactory df = new DataFactory();

    private int COUNT = 1000000;

    public DataGeneratorService() {

//
//        System.out.println("Generowanie i zapis osób");
//        final List<Person> peopleForCar = personRepository.saveAll(generatePersons(COUNT));;
//        System.out.println("Generowanie i zapis samochodów");
//        final List<Car> carList = carRepository.saveAll(generateCars(COUNT));
//        System.out.println("Generowanie ubezpieczeń samochodowych");
//        List<CarInsurance> carInsurances = generateCarInsurances(COUNT);
//        AtomicInteger i = new AtomicInteger();
//
//        carInsurances.forEach( insurance -> {
//            insurance.setCar(carList.get(i.get()));
//            insurance.setOwner(peopleForCar.get(i.getAndIncrement()));
//        });
//        System.out.println("Zapis ubezpieczeń samochodowych");
//        carInsurances = carInsuranceRepository.saveAll(carInsurances);
//
//        System.out.println("Generowanie zdarzeń dla samochodów");
//        List<Issue> issues = generateIssues(COUNT);
//        for ( int it = 0; it < issues.size(); ++it) {
//            issues.get(it).setCarInsurance(carInsurances.get(it));
//        }
//        System.out.println("Zapis zdarzeń samochodów");
//        issueRepository.saveAll(issues);
//
//        System.out.println("Generowanie i zapis osób dla nieruchomości");
//        final List<Person> peopleForHouse = personRepository.saveAll(generatePersons(COUNT));;
//        System.out.println("Generowanie i zapis nieruchomości");
//        final List<House> houseList = houseRepository.saveAll(generateHouses(COUNT));
//        System.out.println("Generowanie ubezpieczeń nieruchomości");
//        List<HouseInsurance> houseInsurances = generateHouseInsurances(COUNT);
//        i.set(0);
//        houseInsurances.forEach( houseInsurance -> {
//            houseInsurance.setHouse(houseList.get(i.get()));
//            houseInsurance.setOwner(peopleForHouse.get(i.getAndIncrement()));
//        });
//        System.out.println("Zapis ubezpieczeń nieruchomości");
//        List<HouseInsurance> houseInsurances1 = houseInsuranceRepository.saveAll(houseInsurances);
//
//        System.out.println("Generowanie zdarzeń dla nieruchomości");
//        issues = generateIssues(COUNT);
//        for ( int it = 0; it < issues.size(); ++it) {
//            issues.get(it).setHouseInsurance(houseInsurances1.get(it));
//        }
//        System.out.println("Zapisywanie zdarzeń dla nieruchomości");
//        issueRepository.saveAll(issues);
//
//        System.out.println("Generowanie osób i zapis dla ubezpieczeń zdrowotnych");
//        final List<Person> peopleForHealth = personRepository.saveAll(generatePersons(COUNT));;
//        System.out.println("Generowanie ubezpieczeń zdrowotnych i stanów zdrowia");
//        List<HealthInsurance> healthInsurances = generateHealthInsurances(COUNT);
//        i.set(0);
//        healthInsurances.forEach(healthInsurance -> {
//            healthInsurance.setOwner(peopleForHealth.get(i.getAndIncrement()));
//            healthInsurance.setPersonHealth(generatePersonHealth(3, healthInsurance));
//        });
//        System.out.println("Zapis ubezpieczeń zdrowotnych i stanów zdrowia");
//        List<HealthInsurance> healthInsurances1 = healthInsuranceRepository.saveAll(healthInsurances);
//        System.out.println("Generowanie zdarzeń zdrowotnych");
//        issues = generateIssues(COUNT);
//        for ( int it = 0; it < issues.size(); ++it) {
//            issues.get(it).setHealthInsurance(healthInsurances1.get(it));
//        }
//        System.out.println("Zapisywanie zdarzeń zdrowotnych");
//        issueRepository.saveAll(issues);


    }


    private List<Person> generatePersons(int count) {
        List<Person> personList = new ArrayList<>(count);

        for ( int i = 0; i < count; ++i) {
            Person p = Person.builder()
                        .address(generateAddress())
                        .birthDate(generateBirthDate())
                        .email(df.getEmailAddress())
                        .firstName(df.getFirstName())
                        .lastName(df.getLastName())
                        .sex(df.getNumberBetween(0, 200) % 2)
                        .sex(df.getNumberBetween(0, 200) % 2)
                        .telephone(generatePhoneNumber())
                        .personType(df.getNumberBetween(0, 500) % 100 == 3 ? "Agent" : "Client")
                        .build();

            personList.add(p);
        }

        return personList;
    }

    private List<Car> generateCars(int count) {
        List<Car> carList = new ArrayList<>(count);
        Date from = new Date();
        from.setYear(80);

        for (int i = 0; i < count; ++i) {
            Car car = Car.builder()
                        .brand(DataSet.getRandomCarBrand())
                        .doors(df.getNumberBetween(2, 5))
                        .firstRegistration(toLocalDate(df.getDateBetween(from, new Date())))
                        .hp(df.getNumberBetween(50, 450))
                        .mileage(df.getNumberBetween(0, 350000))
                        .people(df.getNumberBetween(2, 8))
                        .model(getRandomCarModels())
                        .prodDate(toLocalDate(df.getDateBetween(from, new Date())))
                        .registrationNumber(df.getRandomText(2) + df.getNumberText(5))
                        .vin(df.getRandomText(20))
                        .weight(df.getNumberBetween(700, 2000))
                        .build();
            carList.add(car);
        }

        return carList;
    }

    private List<CarInsurance> generateCarInsurances(int count) {
        List<CarInsurance> carInsurances = new ArrayList<>(count);

        for ( int i = 0; i < count; ++i) {
            int year = df.getNumberBetween(1980, 2020);
            int cost = df.getNumberBetween(500, 100000);
            CarInsurance insurance = CarInsurance.builder()
                                .cost(new BigDecimal(cost))
                                .startDate(LocalDate.of(year, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                                .endDate(LocalDate.of(year + 1, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                                .guarantee(new BigDecimal(cost * 50))
                                .packet(df.getRandomWord())
                                .insuranceType("Ubezpieczenie OC")
                                .build();

            carInsurances.add(insurance);
        }

        return carInsurances;
    }


    private List<Issue> generateIssues(int count) {
        List<Issue> issues = new ArrayList<>(count);

        for ( int i = 0; i < count; ++i) {
            int year = df.getNumberBetween(1980, 2020);
            Issue issue = Issue.builder()
                            .issueCost(new BigDecimal(df.getNumberBetween(500, 25000)))
                            .issueDate(LocalDate.of(year, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                            .issuePlace(generateAddress())
                            .issueSubject(df.getRandomWord())
                            .issueType("Zniszczenie")
                            .sufferedPolicyNumber(df.getNumberText(20))
                            .build();

            issues.add(issue);
        }

        return issues;
    }

    private List<House> generateHouses(int count) {
        List<House> houseList = new ArrayList<>(count);
        int area = df.getNumberBetween(35, 350);
        int year = df.getNumberBetween(1980, 2020);

        for (int i = 0; i < count; ++i) {
            House house = House.builder()
                    .address(generateAddress())
                    .area(area)
                    .assessment(new BigDecimal(area * 15000))
                    .dateOfAssessment(LocalDate.of(year, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                    .address(generateAddress())
                    .build();

            houseList.add(house);
        }

        return houseList;
    }

    private List<HouseInsurance> generateHouseInsurances(int count) {
        List<HouseInsurance> houseInsurances = new ArrayList<>(count);

        for ( int i = 0; i < count; ++i) {
            int year = df.getNumberBetween(1980, 2020);
            int cost = df.getNumberBetween(500, 100000);
            HouseInsurance insurance = HouseInsurance.builder()
                    .cost(new BigDecimal(cost))
                    .startDate(LocalDate.of(year, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                    .endDate(LocalDate.of(year + 1, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                    .guarantee(new BigDecimal(cost * 50))
                    .packet(df.getRandomWord())
                    .insuranceType("Ubezpieczenie od zniszczeń")
                    .build();

            houseInsurances.add(insurance);
        }

        return houseInsurances;
    }

    private List<PersonHealth> generatePersonHealth(int count, HealthInsurance healthInsurance) {
        List<PersonHealth> personHealths = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            int year = df.getNumberBetween(1980, 2020);
            PersonHealth health = PersonHealth.builder()
                    .diseasePast(df.getRandomWord())
                    .diseaseCurrent(df.getRandomWord())
                    .lastTest(LocalDate.of(year + 1, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                    .healthInsurance(healthInsurance)
                    .build();

            personHealths.add(health);
        }

        return personHealths;
    }

    private List<HealthInsurance> generateHealthInsurances(int count) {
        List<HealthInsurance> healthInsurances = new ArrayList<>(count);

        for ( int i = 0; i < count; ++i) {
            int year = df.getNumberBetween(1980, 2020);
            int cost = df.getNumberBetween(500, 100000);
            HealthInsurance insurance = HealthInsurance.builder()
                    .cost(new BigDecimal(cost))
                    .startDate(LocalDate.of(year, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                    .endDate(LocalDate.of(year + 1, df.getNumberBetween(1, 12), df.getNumberBetween(1, 28)))
                    .guarantee(new BigDecimal(cost * 50))
                    .packet(df.getRandomWord())
                    .insuranceType("Ubezpieczenie zdrowotne")
                    .build();

            healthInsurances.add(insurance);
        }

        return healthInsurances;
    }




    private Address generateAddress() {
        Address a = new Address();
        a.setCity(df.getCity());
        a.setPostalCode(generatePostalCode());
        a.setStreet(df.getStreetName() + " " + df.getNumberBetween(1, 300));
        return a;
    }

    private String generatePostalCode() {
        return df.getNumberText(2) + "-" + df.getNumberText(3);
    }

    private LocalDate generateBirthDate() {
        return df.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private String generatePhoneNumber() {
        return df.getNumberText(3) + " " + df.getNumberText(3) + " " + df.getNumberText(3);
    }



}
