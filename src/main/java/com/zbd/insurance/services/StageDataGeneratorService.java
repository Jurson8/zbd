package com.zbd.insurance.services;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.zbd.insurance.dataset.DataSet;
import com.zbd.insurance.entity.*;
import com.zbd.insurance.entity.stage.*;
import com.zbd.insurance.repository.stage.*;
import org.aspectj.weaver.ast.Call;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static com.zbd.insurance.dataset.DataSet.getRandomCarModels;

@Service
public class StageDataGeneratorService {

    private DataFactory df = new DataFactory();

    private int COUNT = 100;

    public StageDataGeneratorService(final ClientRepo clientRepo, final InsuranceRepo insuranceRepo,
                                     final PropertyRepo propertyRepo, final VehicleRepo vehicleRepo,
                                     final TravelRepo travelRepo, final DuesRepo duesRepo, final IssueStageRepo issueStageRepo) {

        ExecutorService executorService = Executors.newFixedThreadPool(8);
        for (final AtomicInteger it = new AtomicInteger(); it.get() < 100; it.getAndIncrement()) {
            final int zz = it.intValue();
            Runnable r = ( () -> {
                int z = zz;
                long a = new Date().getTime();
                List<Client> clients = generateClients(COUNT);
                List<Vehicle> vehicleList = generateVehicle(COUNT);
                List<Property> propertyList = generateProperties(COUNT);
                List<Travel> travelList = generateTravels(COUNT);
                int i = 0;
                List<Insurance> insuranceList = new ArrayList<>();
                System.out.println("generowanie ubezpieczeń");
                for (Client client : clients) {
                    Insurance insurance = generateInsurances(1).get(0);
                    insurance.setInsuranceSubject("Dom");
                    insurance.addClient(client);
                    insurance.addIssue(generateIssueStage(1, "Pożar").get(0));
                    insurance.addProperty(propertyList.get(i));
                    generateDues(df.getNumberBetween(1, 5)).forEach(insurance::addDues);
                    insuranceList.add(insurance);

                    insurance = generateInsurances(1).get(0);
                    insurance.setInsuranceSubject("Samochód");
                    insurance.addClient(client);
                    insurance.addIssue(generateIssueStage(1, "Wypadek").get(0));
                    insurance.addVehicle(vehicleList.get(i));
                    generateDues(df.getNumberBetween(1, 5)).forEach(insurance::addDues);
                    insuranceList.add(insurance);

                    insurance = generateInsurances(1).get(0);
                    insurance.setInsuranceSubject("Podróż");
                    insurance.addClient(client);
                    insurance.addIssue(generateIssueStage(1, "Opóźnienia lotnicze").get(0));
                    insurance.addTravel(travelList.get(i++));
                    generateDues(df.getNumberBetween(1, 5)).forEach(insurance::addDues);
                    insuranceList.add(insurance);
                }

                System.out.println("Zapisywanie ubezpieczeń");
                insuranceRepo.saveAll(insuranceList);
                System.out.println("Zadanie zakończone: " + z + " z czasem " + String.valueOf(new Date().getTime() - a) + "ms.");
            });
            executorService.submit(r);
        }


    }


    private List<IssueStage> generateIssueStage(int count, String issueType) {
        List<IssueStage> issueStageList = new ArrayList<>(count);
        Date from = new Date();
        from.setYear(80);

        for (int i = 0; i < count; ++i) {
            IssueStage issue = IssueStage.builder()
                    .address(generateAddress())
                    .date(toLocalDate(df.getDateBetween(from, new Date())))
                    .type(issueType)
                    .build();

            Compensation compensation = Compensation.builder()
                    .amount(new BigDecimal(df.getNumberBetween(0, 30000)))
                    .issue(issue)
                    .build();
            issue.setCompensation(compensation);

            issueStageList.add(issue);
        }

        return issueStageList;
    }

    private List<Dues> generateDues(int count) {
        List<Dues> duesList = new ArrayList<>(count);

        for (int i = 0; i < count; ++i) {
            Dues insurance = Dues.builder()
                    .amount(new BigDecimal(df.getNumberBetween(200, 5600)))
                    .build();
            duesList.add(insurance);
        }

        return duesList;
    }

    private List<Insurance> generateInsurances(int count) {
        List<Insurance> insuranceList = new ArrayList<>(count);

        for (int i = 0; i < count; ++i) {
            Insurance insurance = Insurance.builder()
                    .insuranceValue(df.getNumberBetween(10000, 1000000))
                    .build();
            insuranceList.add(insurance);
        }

        return insuranceList;
    }

    private List<Client> generateClients(int count) {
        List<Client> clientList = new ArrayList<>(count);

        for (int i = 0; i < count; ++i) {
            Client car = Client.builder()
                    .address(generateAddress())
                    .birthDate(toLocalDate(df.getBirthDate()))
                    .sex(df.getNumberBetween(0, 200) % 2 == 1)
                    .name(df.getFirstName())
                    .surname(df.getLastName())
                    .build();
            clientList.add(car);
        }

        return clientList;
    }

    private List<Vehicle> generateVehicle(int count) {
        List<Vehicle> vehicleList = new ArrayList<>(count);
        Date from = new Date();
        from.setYear(80);

        for (int i = 0; i < count; ++i) {
            Vehicle car = Vehicle.builder()
                        .brand(DataSet.getRandomCarBrand())
                        .doors(df.getNumberBetween(2, 5))
                        .power(df.getNumberBetween(50, 450))
                        .mileage(df.getNumberBetween(0, 350000))
                        .people(df.getNumberBetween(2, 8))
                        .prodDate(toLocalDate(df.getDateBetween(from, new Date())))
                        .registrationNumber(df.getRandomText(2) + df.getNumberText(5))
                        .vin(df.getRandomText(20))
                        .weight(df.getNumberBetween(700, 2000))
                        .type(DataSet.vehicleType.get(df.getNumberBetween(0, DataSet.vehicleType.size()-1)))
                        .build();
            vehicleList.add(car);
        }

        return vehicleList;
    }

    private List<Property> generateProperties(int count) {
        List<Property> propertyList = new ArrayList<>(count);

        for (int i = 0; i < count; ++i) {
            Property car = Property.builder()
                    .address(generateAddress())
                    .area(df.getNumberBetween(35, 350))
                    .build();
            propertyList.add(car);
        }

        return propertyList;
    }

    private List<Travel> generateTravels(int count) {
        List<Travel> travelList = new ArrayList<>(count);

        for (int i = 0; i < count; ++i) {
            Travel car = Travel.builder()
                    .country(DataSet.countries.get(df.getNumberBetween(0, DataSet.countries.size() -1)))
                    .duration(df.getNumberBetween(1, 90))
                    .build();
            travelList.add(car);
        }

        return travelList;
    }

    private Address generateAddress() {
        Address a = new Address();
        a.setCity(df.getCity());
        a.setPostalCode(generatePostalCode());
        a.setStreet(df.getStreetName() + " " + df.getNumberBetween(1, 300));
        return a;
    }

    private String generatePostalCode() {
        return df.getNumberText(2) + "-" + df.getNumberText(3);
    }

    private LocalDate generateBirthDate() {
        return df.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private String generatePhoneNumber() {
        return df.getNumberText(3) + " " + df.getNumberText(3) + " " + df.getNumberText(3);
    }



}
