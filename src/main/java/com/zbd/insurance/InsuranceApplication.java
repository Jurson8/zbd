package com.zbd.insurance;

import com.zbd.insurance.services.DataGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceApplication {

    @Autowired
    DataGeneratorService dataGeneratorService;

    public static final String schema = "orakiel";


    public static void main(String[] args) {
        SpringApplication.run(InsuranceApplication.class, args);
    }

}
